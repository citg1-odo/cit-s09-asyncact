package com.zuitt.discussion;

import org.apache.coyote.Request;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
// This application will function as an endpoint that will be used in handling http request.
@RestController
//Will require all routes within the class to use the set endpoint as part of its route.
@RequestMapping("/greeting")
public class DiscussionApplication {
	private ArrayList<Student> students = new ArrayList<Student>();
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/hello")
//	Maps a get request to the route "/hello" and invokes the method hello().
	public String hello(){
		return "Hello World";
	}

	@GetMapping("/hi") //localhost:8080/hi?name=value
//	@RequestParam annotation that allows us to extract data from query strings in the URL
	public String hi(@RequestParam(value = "name", defaultValue = "John") String name){
		return String.format("Hi %s", name);
	}

//	Multiple Parameters
//	localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name,
						 @RequestParam(value = "friend", defaultValue = "Jane") String friend){
		return String.format("Hello %s! My name is %s.", friend, name);
	}

//	Route with path variables
//	Dynamic data is obtained directly from the url
//	localhost:8080/hello
	@GetMapping("hello/{name}")
//	@PathVariable annotation allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name") String name){
		return String.format("Nice to meet you %s", name);
	}


//  S09 Synchronous Activity
	@GetMapping("/welcome")
	public String welcome(@RequestParam(value = "user") String user,
						  @RequestParam(value = "role") String role){

		switch (role) {
			case "admin":
				return "Welcome back to the class portal, Admin "+ user +"!";
			case "teacher":
				return "Thank you for logging in, Teacher "+ user +"!";
			case "student":
				return "Welcome to the class portal, "+ user +"!";
			default:
				return  "Role out of range!";
		}
	}

	@GetMapping("/register")
	public String register(@RequestParam(value = "id") String id,
						   @RequestParam(value = "name") String name,
						   @RequestParam(value = "course") String course){
		Student student = new Student(Integer.parseInt(id), name, course);
		students.add(student);
		return id+" your id number is registered on the system!";
	}

	@GetMapping("/account/{id}")
	public String account(@PathVariable("id") String id){
		Student student = null;
		for (Student s : students) {
			if (s.getId() == Integer.parseInt(id)) {
				student = s;
				break;
			}
		}
		if(student == null)
			return "Your provided "+ id +" is not found in the system!";
		else
			return "Welcome back "+ student.getName() +"! You are currently enrolled in "+ student.getCourse();
	}
	
}